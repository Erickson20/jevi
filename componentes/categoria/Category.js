import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView
  
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import ProductBox from '../product/ProductBox';
import {Actions} from 'react-native-router-flux'




export default class Category extends Component {
    

  render() {
  
    
    return (
     <View style={{backgroundColor: 'white', flex: 1}}>
   
     <View style={{marginTop: 10, backgroundColor: 'white', flexDirection: 'row', flex: 1}}>
     <ProductBox/>
     <ProductBox/>
     <ProductBox/>
     </View>
     <View style={{marginTop: 10, backgroundColor: 'white', flexDirection: 'row', flex: 3}}>
     <ProductBox/>
     <ProductBox/>
     <ProductBox/>
     </View>
     </View>
    );
  }
}



const styles = StyleSheet.create({
    wrapper: {
    },
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#9DD6EB',
    },
    slide2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#97CAE5',
    },
    slide3: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#92BBD9',
    },
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold',
    }
  })
  