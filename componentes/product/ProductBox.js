import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  ImageBackground
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux'
import Modal from "react-native-modal";




export default class ProductBox extends Component{

  state = {
    isModalVisible: false,
    cantidad: 0
  };
  _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });
  render() {
    return (
      <View  style={{  padding: 5}}>
        
      <View style={{   justifyContent: 'center',  alignItems: 'center', width: 112, height: 140,  flexDirection: 'column'}}>
          <TouchableHighlight onPress={() => this._toggleModal()}>
              <Image  source={{uri: 'https://www.glengarrywines.co.nz/images/v9/bottles/93310.png'}} style={{borderRadius: 5, height: 120, width: 65}}/>
          </TouchableHighlight>
          <Text style={{color: '#B1181A', fontSize: 12}}>Wisky Black Label</Text>
          <Text style={{color: '#B1181A', fontSize: 10}}>RD$1200</Text>
      
  </View>

  <Modal isVisible={this.state.isModalVisible} >
      <View style={{ padding: 10, height: 125, backgroundColor: 'white',  justifyContent: 'space-between', flexDirection: 'row' }}>

      <View style={{ flexDirection: 'column' }}>
      <Image  source={{uri: 'https://www.glengarrywines.co.nz/images/v9/bottles/93310.png'}} style={{borderRadius: 5, height: 120, width: 65}}/>
      </View>

      <View style={{ flexDirection: 'column', marginRight: 15, paddingTop: 10, alignItems: 'center' }}>
      <Text style={{color: '#B1181A', fontSize: 20, textAlign: 'center'}}>Wisky Black Label</Text>
      <Text style={{ fontSize: 15, textAlign: 'center'}}>Wisky Black Label 1LT</Text>
      <Text style={{color: '#B1181A', fontSize: 20, textAlign: 'center'}}>$RD 1200</Text>

      </View>

      </View>

      <View style={{ height: 48, padding: 10, backgroundColor: 'white', justifyContent: 'space-around', flexDirection: 'row' }}>
                <TouchableOpacity style={{alignItems: 'center', justifyContent: 'center', width: 40, height: 40}} onPress={() => this.resta()} >
                   <Icon name='minus' size={25} style={{color:'#B1181A' }}/>
                </TouchableOpacity>
                <Text style={{fontSize: 25,   color: '#B1181A'}}>{this.state.cantidad}</Text>
                <TouchableOpacity style={{alignItems: 'center', justifyContent: 'center', width: 40, height: 40}} onPress={() => this.sumar()} >
                   <Icon name='plus' size={25} style={{color:'#B1181A'}}/>
                </TouchableOpacity>
              
      </View>
      <View style={{ padding: 10, height: 60, backgroundColor: 'white', justifyContent: 'space-between', flexDirection: 'row' }}>
                <TouchableOpacity  style={styles.button}>
                  <Text style={{  color: 'white'}}>Comprar <Icon name='shopping-cart' size={18} style={{color:'white'}}/></Text>
              </TouchableOpacity>
              <TouchableOpacity  onPress={() => this._toggleModal()}  style={{   borderColor: '#B1181A', borderWidth: 2, alignItems: 'center', justifyContent: 'center', borderRadius: 1, elevation: 3, height: 40, width: 100,  backgroundColor: 'white',}}>
                  <Text style={{   color: '#B1181A'}}>Cerrar</Text>
              </TouchableOpacity>
      </View>
    </Modal>

   </View>
    
    );
  }

  sumar() {
    var contador = this.state.cantidad + 1;
    this.setState({
      cantidad: contador
    });

  }

  resta(){
    if(this.state.cantidad > 0) {
    var contador = this.state.cantidad - 1;
    this.setState({
      cantidad: contador
    });
    }
  }

  

 

}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
   
  },
  button: {
    borderColor: '#B1181A',
     borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 1,
     elevation: 3,
      height: 40,
       width: 150, 
        
        backgroundColor: '#B1181A',
        
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
