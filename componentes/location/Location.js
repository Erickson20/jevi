import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView
  
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import ProductBox from '../product/ProductBox';
import {Actions} from 'react-native-router-flux'




export default class Location extends Component {
    

  render() {
  
    
    return (
     <View style={{backgroundColor: 'white', flex: 1, flexDirection: 'column'}}>
   
     
     <View style={{ backgroundColor: 'white', flexDirection: 'column', justifyContent: 'center'}}>
     <Image
     style={{width: 400, height: 350}}
     source={{uri: 'https://firebasestorage.googleapis.com/v0/b/jevi-9acff.appspot.com/o/logo%2FCaptura.PNG?alt=media&token=36f1329b-2cb2-429b-8eb0-6cd01a9dc38d'}}
     />
     </View>

     <View style={{ backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-around'}}>
                 <TouchableOpacity  style={styles.button}>
                    <Text style={{  color: 'white'}}>Localizame</Text>
                </TouchableOpacity>

                <TouchableOpacity  onPress={() => Actions.Payment()}  style={{   marginRight: 5,borderColor: '#B1181A', borderWidth: 1, alignItems: 'center', justifyContent: 'center', borderRadius: 1, elevation: 3, height: 40, width: 150, marginTop: 40, backgroundColor: 'white',}}>
                    <Text style={{   color: '#B1181A'}}>Siguiente</Text>
                </TouchableOpacity>
     </View>

     <View style={{marginTop: 50, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-around'}}>
                <Text style={{color: '#B1181A'}}>No pueden localizarte?</Text>
     </View>

     

     
                 
     
     </View>
    );
  }
}



const styles = StyleSheet.create({
    wrapper: {
    },
    button: {
        borderColor: '#B1181A',
         borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 1,
         elevation: 3,
          height: 40,
           width: 150, 
            marginTop: 40,
            marginLeft: 5,
            backgroundColor: '#B1181A',
            
      },
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#9DD6EB',
    },
    slide2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#97CAE5',
    },
    slide3: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#92BBD9',
    },
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold',
    }
  })
  