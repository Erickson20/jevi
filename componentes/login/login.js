import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  ImageBackground
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux'

const Correo = Platform.select({
  ios: 
   <TextInput
    style={{padding: 10 , height: 40, width: 300, borderRadius: 4, borderColor: '#B1181A', borderWidth: 1, color: '#B1181A', marginTop: 30}}
    placeholderTextColor='#B1181A'
     underlineColorAndroid='#B1181A'
     placeholder="Correo"
    
    />
   
  ,
  android: 
    <TextInput
    style={{ height: 40, width: 300,  color: '#B1181A', marginTop: 30}}
    placeholderTextColor='#B1181A'
     underlineColorAndroid='#B1181A'
     placeholder="Correo"
    
    />
  ,
});
const Clave = Platform.select({
  ios: 
  <TextInput
  style={{padding: 10 ,height: 40, width: 300,  borderRadius: 4, borderColor: '#B1181A', borderWidth: 1,  color: '#B1181A', marginTop: 30}}
  placeholderTextColor='#B1181A'
   underlineColorAndroid='#B1181A'
   placeholder="Contraseña"
   secureTextEntry={true}
  />
    
  ,
  android: 
  <TextInput
  style={{height: 40, width: 300,  color: '#B1181A', marginTop: 30}}
  placeholderTextColor='#B1181A'
   underlineColorAndroid='#B1181A'
   placeholder="Contraseña"
   secureTextEntry={true}
  />
  ,
});


export default class login extends Component {
  render() {
    return (
      
      <ImageBackground
          source={require('../img/fondo.jpg')} style={styles.container}
      >
            <Image
            style={{width: 300, height: 150,  marginTop: 30}}
            source={{uri: 'https://firebasestorage.googleapis.com/v0/b/jevi-9acff.appspot.com/o/logo%2Fjevi%20recortado.png?alt=media&token=71eb125b-ba71-4bdc-af9d-c876bca2d742'}}
            />
            
            {Correo}
            {Clave}
           
             
            <View style={{flex: 1, flexDirection: 'row'}}>
                <TouchableOpacity onPress={() => Actions.drawerMenu()}  style={styles.button}>
                    <Text style={{  color: 'white'}}>Entra</Text>
                </TouchableOpacity>
                <TouchableOpacity  onPress={() => Actions.Register()}  style={{   marginRight: 5,borderColor: '#B1181A', borderWidth: 2, alignItems: 'center', justifyContent: 'center', borderRadius: 1, elevation: 3, height: 40, width: 150, marginTop: 40, backgroundColor: 'white',}}>
                    <Text style={{   color: '#B1181A'}}>Registrarte</Text>
                </TouchableOpacity>
                
            </View>

            <View style={{flex: 1, flexDirection: 'row', marginTop: 30}}>
                    <TouchableHighlight style={{ marginTop: 20, }} onPress={() => Actions.Forget()}>
                <Text style={{color:'#B1181A'}}>Se me olvido la clave </Text>
                </TouchableHighlight>

                
            </View>
            <View style={{ flex: 2, flexDirection: 'row', marginTop: 50}}>
                    <TouchableOpacity    style={{alignItems: 'center', justifyContent: 'center', borderRadius: 1, elevation: 3, height: 40, width: 130, marginTop: 40, backgroundColor: 'white'}}>
                    <Icon name="facebook" color="#B1181A" size={30}  />
                    </TouchableOpacity>
                    <TouchableOpacity>
                    <Text>- o -</Text>
                    </TouchableOpacity>
                    <TouchableOpacity    style={{ alignItems: 'center', justifyContent: 'center', borderRadius: 1, elevation: 3, height: 40, width: 130, marginTop: 40, backgroundColor: 'white'}}>
                    <Icon name="google" color="#B1181A" size={30}  />
                    </TouchableOpacity>
                    
            </View>
            <View style={{flex: 1, flexDirection: 'row', marginTop: 30}}>
                
                <Text style={{color:'white'}}>By Domcam Technology</Text>
                
            </View>
            </ImageBackground>
     
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
   
  },
  button: {
    borderColor: '#B1181A',
     borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 1,
     elevation: 3,
      height: 40,
       width: 150, 
        marginTop: 40,
        marginLeft: 5,
        backgroundColor: '#B1181A',
        
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
