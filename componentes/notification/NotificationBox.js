import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  ImageBackground
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux'
import Modal from "react-native-modal";




export default class NotificationBox extends Component {


  render() {
    return (
      <View  style={{  padding: 5}}>
        
      <View style={{ padding: 10, backgroundColor: 'white', elevation: 3, borderRadius: 5,    justifyContent: 'center',  alignItems: 'center', width: 350, height: 110,  flexDirection: 'row'}}>
          <TouchableHighlight >
              <Image  source={{uri: 'https://i.pinimg.com/originals/f8/e2/e2/f8e2e2fe5362930a4d387ade806a82a1.jpg'}} style={{borderRadius: 5, height: 100, width: 100}}/>
          </TouchableHighlight>
          <View style={{ padding: 10,   justifyContent: 'center',  alignItems: 'center', width: 240, height: 160,  flexDirection: 'column'}}>
          <Text style={{color: '#B1181A', fontSize: 14}}>Hoy es viernes y el cuerpo lo sabe!</Text>
          <Text style={{ marginTop: 10, fontSize: 12}}>Hoy ponte jevi con 5 cervezas en un cubetazo rompe la monotonia con esta oferta</Text>
          <TouchableOpacity  onPress={() => Actions.pop()}  style={{   marginLeft: 140 ,borderColor: '#B1181A', borderWidth: 1, alignItems: 'center', justifyContent: 'center', borderRadius: 1, elevation: 3, height: 25, width: 60, backgroundColor: 'white',}}>
          <Text style={{   color: '#B1181A'}}>Ver</Text>
        </TouchableOpacity>
          </View>
         
      
  </View>

  

   </View>
    
    );
  }

 

  

 

}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
   
  },
  button: {
    borderColor: '#B1181A',
     borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 1,
     elevation: 3,
      height: 40,
       width: 150, 
        
        backgroundColor: '#B1181A',
        
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
