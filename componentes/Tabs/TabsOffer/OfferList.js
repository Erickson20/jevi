import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  ImageBackground,
  ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux'
import OfferBox from '../TabsOffer/OfferBox';




export default class OfferList extends Component {
  render() {
    return (
        <ScrollView style={{backgroundColor: 'white', flex: 1}}>
        <Text style={{marginTop: 10, fontSize: 20, textAlign: 'center', color: '#B1181A'}}>Ofertas OnFire! <Icon name='fire' size={25} style={{color:'#B1181A' }}/></Text>
            <View style={{marginTop: 10, backgroundColor: 'white', flexDirection: 'row', flex: 1}}>
            
                <OfferBox/>
                <OfferBox/>
                <OfferBox/>
            
            </View>
            <View style={{marginTop: 10, backgroundColor: 'white', flexDirection: 'row', flex: 1}}>
            
                <OfferBox/>
                <OfferBox/>
                <OfferBox/>
            
            </View>
           
        
            
     </ScrollView>
      
    );
  }
}

