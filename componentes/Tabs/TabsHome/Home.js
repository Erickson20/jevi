import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  
  ScrollView
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import autoPlay from 'react-swipeable-views-utils/lib/autoPlay';
import SwipeableViews from 'react-swipeable-views-native';

import { Actions } from 'react-native-router-flux';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);


export default class Home extends Component {
  render() {
    return (
      
      
          

          <ScrollView style={styles.container}>
          
          <AutoPlaySwipeableViews  style={styles.slideContainer}>
          <View style={[styles.slide, styles.slide1]}>
          <Image source={{uri: 'http://www.puravidaalmerimar.es/wp-content/uploads/2013/07/banner-fb-brugal.png'}} style={{ height: 150, width: null}}/>

          </View>
          <View style={[styles.slide, styles.slide2]}>
          <Image source={{uri: 'https://www.aeliadutyfree.co.nz/media/wysiwyg/0074_HOW_Banner_1160x400px.jpg'}} style={{ height: 150, width: null}}/>

          </View>
          <View style={[styles.slide, styles.slide3]}>
          <Image source={{uri: 'http://sibaritas-club.com/wp-content/uploads/2015/11/banner-home3.png'}} style={{ height: 150, width: null}}/>

          </View>
        </AutoPlaySwipeableViews>


          <View style={{ flex: 1}}>
              <View style={{borderColor: 'white', borderWidth: 0, justifyContent: 'center',  alignItems: 'center', backgroundColor: '#B1181A', elevation: 0}}>
                  <Text style={{color: 'white'}}>Categorias</Text>
              </View>
             
          </View>
          
          <View style={{ padding: 5, justifyContent: 'space-between', flexDirection: 'row'}}>
              <View style={{ borderColor: '#B1181A', borderWidth: 1, borderRadius: 5,  justifyContent: 'center',  alignItems: 'center', width: 112, height: 140, backgroundColor: 'white', elevation: 4,  flexDirection: 'column'}}>
                  <Image source={{uri: 'https://firebasestorage.googleapis.com/v0/b/jevi-9acff.appspot.com/o/categorias%2From_00000.png?alt=media&token=1b944919-3f9e-4d13-a1a3-34d68f7f91ab'}} style={{ height: 110, width: 100}}/>
                  <Text style={{color: '#B1181A'}}>Rom</Text>
              </View>
              
              <View style={{borderColor: '#B1181A', borderWidth: 1, borderRadius: 5,  justifyContent: 'center',  alignItems: 'center',width: 112, height: 140, backgroundColor: 'white', elevation: 4}}>
              <TouchableHighlight onPress={() => Actions.Category()}>
                  <Image source={{uri: 'https://firebasestorage.googleapis.com/v0/b/jevi-9acff.appspot.com/o/categorias%2Fwjy_00000.png?alt=media&token=e7211fa3-4f42-46a0-8e13-64d06dd882cd'}} style={{ height: 110, width: 100}}/>
              </TouchableHighlight>
                  <Text style={{color: '#B1181A'}}>Whisky</Text>
                
                  </View>
              <View style={{ borderColor: '#B1181A', borderWidth: 1, borderRadius: 5,  justifyContent: 'center',  alignItems: 'center', width: 112, height: 140, backgroundColor: 'white', elevation: 4}}>
                  <Image source={{uri: 'https://firebasestorage.googleapis.com/v0/b/jevi-9acff.appspot.com/o/categorias%2Fvino_00000.png?alt=media&token=b11a80f8-4519-4900-8463-3c676cb3d8f0'}} style={{ height: 110, width: 100}}/>
                  <Text style={{color: '#B1181A'}}>Vino</Text>
              </View>
          </View>
      
          <View style={{padding: 5, justifyContent: 'space-between', flex: 1, flexDirection: 'row'}}>
              <View style={{ borderColor: '#B1181A', borderWidth: 1, borderRadius: 5,  justifyContent: 'center',  alignItems: 'center',width: 112, height: 140, backgroundColor: 'white', elevation: 4}}>
                  <Image source={{uri: 'https://firebasestorage.googleapis.com/v0/b/jevi-9acff.appspot.com/o/categorias%2FComp%201_00000.png?alt=media&token=130c4040-df81-43ce-857a-60c453ce7a67'}} style={{ height: 110, width: 100}}/>
                  <Text style={{color: '#B1181A'}}>Sidra</Text>
              </View>
              <View style={{  borderColor: '#B1181A', borderWidth: 1, borderRadius: 5, justifyContent: 'center',  alignItems: 'center',width: 112, height: 140, backgroundColor: 'white', elevation: 4}}>
                  <Image source={{uri: 'https://firebasestorage.googleapis.com/v0/b/jevi-9acff.appspot.com/o/categorias%2Fhene_00000.png?alt=media&token=7d6496d6-baf0-4c32-93c4-2ccb1bc8c969'}} style={{ height: 110, width: 100}}/>
                  <Text style={{color: '#B1181A'}}>Coñac</Text>
              </View>
              <View style={{ borderColor: '#B1181A', borderWidth: 1, borderRadius: 5, justifyContent: 'center',  alignItems: 'center', width: 112, height: 140, backgroundColor: 'white', elevation: 4}}>
                  <Image source={{uri: 'https://firebasestorage.googleapis.com/v0/b/jevi-9acff.appspot.com/o/categorias%2Fchamp_00000.png?alt=media&token=23c18b4f-6618-489d-8ccc-931fd3d6d8ac'}} style={{ height: 110, width: 100}}/>
                  <Text style={{color: '#B1181A'}}>Shampagne</Text>
              </View>
          </View>

          <View style={{ padding: 5, justifyContent: 'space-between', flex: 1, flexDirection: 'row'}}>
              <View style={{borderColor: '#B1181A', borderWidth: 1, borderRadius: 5, justifyContent: 'center',  alignItems: 'center', width: 112, height: 140, backgroundColor: 'white', elevation: 4}}>
                  <Image source={{uri: 'https://firebasestorage.googleapis.com/v0/b/jevi-9acff.appspot.com/o/categorias%2Fvodka_00000.png?alt=media&token=2609b1a1-0e9a-4782-9bd7-2973396f4544'}} style={{ height: 110, width: 100}}/>
                  <Text style={{color: '#B1181A'}}>Vodak</Text>
              </View>
             
          </View>
            
      </ScrollView>
            
      

      
    );
  }
}



const styles = StyleSheet.create({
   slideContainer: {
    height: 150,
  },
  slide: {
    
    height: 150,
  },
  slide1: {
    backgroundColor: '#FEA900',
  },
  slide2: {
    backgroundColor: '#B3DC4A',
  },
  slide3: {
    backgroundColor: '#6AC0FF',
  },
  text: {
    color: '#fff',
    fontSize: 16,
  },
  container: {
    flex: 1,
    

    backgroundColor: 'white',
   
  }
 
})