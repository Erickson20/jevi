import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  ImageBackground
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux'




export default class TrackBox extends Component {
  render() {
    return (
        <View  style={{  padding: 5}}>
        
        
        <View style={{ backgroundColor: 'white', padding: 5, elevation: 2, marginTop: 10, borderRadius: 2}}>
            
            <View style={{flex: 1, flexDirection: 'row',alignItems: 'center', justifyContent: 'space-between', paddingTop: 10, paddingBottom: 30}}>
              <View style={{ width: 130, height: 20, }}><Text >ID Orden:</Text></View>
              <View style={{  width: 180,  height: 20, alignItems: 'center'  }}><Text style={{ textAlign:'left', color: '#B1181A'}} >5678867 sadd </Text></View>
            </View>

            <View style={{flex: 1, flexDirection: 'row',alignItems: 'center', justifyContent: 'space-between', paddingTop: 10, paddingBottom: 30}}>
              <View style={{ width: 130, height: 20, }}><Text >Cliente:</Text></View>
              <View style={{  width: 180, height: 20, alignItems: 'center' }}><Text style={{  color: '#B1181A'}} >Erickson Flores Bisono </Text></View>
            </View>

            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingTop: 10, paddingBottom: 30}}>
              <View style={{width: 130, height: 20, }}><Text >Fecha de despacho:</Text></View>
              <View style={{width: 180, height: 20, alignItems: 'center' }}><Text style={{ textAlign: 'left', color: '#B1181A'}} >12/14/18 12:34</Text></View>
            </View>

            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingTop: 10, paddingBottom: 30}}>
              <View style={{width: 130, height: 20, }}><Text >Tiempo de entrega:</Text></View>
              <View style={{width: 180, height: 20, alignItems: 'center' }}><Text style={{ textAlign: 'left', color: '#B1181A'}} >5:00 minutos</Text></View>
            </View>

            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingTop: 15, paddingBottom: 15 }}>
            <Icon name='truck' size={25} style={{color:'#B1181A' }}/>
            <Icon name='road' size={25} style={{color:'#B1181A' }}/>
            <Icon name='map-marker' size={25} style={{color:'#B1181A' }}/>
            </View>
            
            <View style={{ backgroundColor: '#B1181A', height: 10, width: 100, borderRadius: 2}}/>
               
            
     </View>

     

    
     </View>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
   
  },
  button: {
    borderColor: '#B1181A',
     borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 1,
     elevation: 3,
      height: 40,
       width: 150, 
        marginTop: 40,
        marginLeft: 10,
        backgroundColor: '#B1181A',
        
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
