import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  ImageBackground,
  Dimensions
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Tabs, Actions, Scene, Router} from 'react-native-router-flux';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';



import Home from '../Tabs/TabsHome/Home';
import Register from '../register/Register';
import Login from '../login/login';
import TrackList from '../Tabs/TabTrack/TrackList';
import OfferList from '../Tabs/TabsOffer/OfferList';


const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

const FirstRoute = () => <Home/>;
const SecondRoute = () => <OfferList/>;
const TreeRoute = () => <TrackList/>;


export default class inicio extends Component {
  state = {
    index: 0,
    routes: [
      { key: 'first',  name: 'home' },
      { key: 'second',  name: 'star' },
      { key: 'Three',  name: 'archive' }
    ],
  };
  

  _renderIcon = ({ route }: any) => {
    return <Icon name={route.name} size={25} color="white" />;
  };

  _handleIndexChange = index => this.setState({ index });

  _renderHeader = props => <TabBar 
  renderIcon={this._renderIcon}
  indicatorStyle={{backgroundColor: 'white'}} style={{backgroundColor: '#B1181A'}} {...props} />;

  _renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    Three: TreeRoute
  });
  render() {
    return (
      <TabViewAnimated
      
      style={{flex: 1}}
      navigationState={this.state}
      renderScene={this._renderScene}
      renderHeader={this._renderHeader}
      onIndexChange={this._handleIndexChange}
      initialLayout={initialLayout}
  
    />
      
    );
  }
}



const styles = StyleSheet.create({

  container: {
    flex: 1,
  

  }

})