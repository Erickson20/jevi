import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView
  
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import ProductBox from '../product/ProductBox';
import {Actions} from 'react-native-router-flux'




export default class Cart extends Component {
    

  render() {
  
    
    return (
     <View style={{backgroundColor: 'white', flex: 1, flexDirection: 'column'}}>
   
     <View style={{marginTop: 10, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-around', flex: 1}}>
        <View style={{ padding: 5 ,borderColor: '#B1181A', borderWidth: 1, flexDirection: 'row', height: 160,}}>
            <ProductBox/>
            <Icon name='times-circle' size={20} style={{ marginLeft: -20,  color:'#B1181A'}}/>
        </View>

        <View style={{padding: 5, borderColor: '#B1181A', borderWidth: 1, height: 150, flexDirection: 'row', height: 160}}>
            <ProductBox/>
            <Icon name='times-circle' size={20} style={{ marginLeft: -20, color:'#B1181A'}}/>
        </View>
   
     </View>

     <View style={{marginTop: 10, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-around', flex: 1}}>
     <View style={{ padding: 5 ,borderColor: '#B1181A', borderWidth: 1, flexDirection: 'row', height: 160,}}>
         <ProductBox/>
         <Icon name='times-circle' size={20} style={{ marginLeft: -20,  color:'#B1181A'}}/>
     </View>

     <View style={{padding: 5, borderColor: '#B1181A', borderWidth: 1, height: 150, flexDirection: 'row', height: 160}}>
         <ProductBox/>
         <Icon name='times-circle' size={20} style={{ marginLeft: -20, color:'#B1181A'}}/>
     </View>

  </View>

     <View style={{marginTop: 10, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'center', flex: 1}}>
        <TouchableOpacity onPress={() => Actions.Location()}  style={styles.button}>
            <Text style={{  color: 'white'}}>Realizar compra</Text>
        </TouchableOpacity>
        
     </View>
                 
     
     </View>
    );
  }
}



const styles = StyleSheet.create({
    wrapper: {
    },
    button: {
        borderColor: '#B1181A',
         borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 1,
         elevation: 3,
          height: 40,
           width: 150, 
            marginTop: 40,
            marginLeft: 5,
            backgroundColor: '#B1181A',
            
      },
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#9DD6EB',
    },
    slide2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#97CAE5',
    },
    slide3: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#92BBD9',
    },
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold',
    }
  })
  