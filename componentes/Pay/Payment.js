import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView
  
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import ProductBox from '../product/ProductBox';
import {Actions} from 'react-native-router-flux'
import Modal from "react-native-modal";




export default class Payment extends Component {
    state = {
        isModalVisible: false,
        cantidad: 0
      };
      _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });

  render() {
  
    
    return (
     <View style={{backgroundColor: 'white', flex: 1, flexDirection: 'column'}}>
   
     
     <View style={{ marginTop: 100, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-around'}}>
        
        <View style={{   borderColor: '#B1181A', width: 150, height: 150, borderWidth: 2, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity  onPress={() => this._toggleModal()}>
            <Icon name='money' size={60} style={{ marginLeft: 20, color:'#B1181A'}}/>
            <Text style={{fontSize: 15}}>Dinero en efectivo</Text>
            
        </TouchableOpacity>
        </View>

        <View style={{borderColor: '#B1181A', width: 150, height: 150, borderWidth: 2, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity  onPress={() => this._toggleModal()}>    
            <Icon name='credit-card' size={60} style={{ marginLeft: 20,  color:'#B1181A'}}/>
            <Text style={{fontSize: 15}}>tarjeta de credito</Text>
        </TouchableOpacity>
        </View>
     
    
     </View>

     <View style={{  backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-around'}}>
                 

                <TouchableOpacity  onPress={() => Actions.Detail()}  style={{   marginRight: 5,borderColor: '#B1181A', borderWidth: 1, alignItems: 'center', justifyContent: 'center', borderRadius: 1, elevation: 3, height: 40, width: 150, marginTop: 40, backgroundColor: '#B1181A',}}>
                    <Text style={{   color: 'white'}}>Siguiente</Text>
                </TouchableOpacity>
     </View>

     <View style={{marginTop: 50, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-around'}}>
                <Text style={{color: '#B1181A'}}>Hay problemas con un metodo de pago?</Text>
     </View>

     

     <Modal isVisible={this.state.isModalVisible} >
        <View style={{ padding: 10, height: 200, backgroundColor: 'white',  justifyContent: 'space-between', flexDirection: 'row' }}>

        

        <View style={{ flexDirection: 'column',}}>
        
        <TextInput
        style={{height: 40, width: 300,  color: '#B1181A'}}
        placeholderTextColor='#D8D8D8'
         underlineColorAndroid='#B1181A'
         placeholder="Nombre de la tarjeta"
        
        />
        <View style={{ flexDirection: 'row',  justifyContent: 'space-between'}}>
        <TextInput
        style={{marginTop: 20, height: 40, width: 130,  color: '#B1181A'}}
        placeholderTextColor='#D8D8D8'
         underlineColorAndroid='#B1181A'
         placeholder="Fecha de exp"
        
        />
        <TextInput
        style={{marginTop: 20, height: 40, width: 130,  color: '#B1181A'}}
        placeholderTextColor='#D8D8D8'
         underlineColorAndroid='#B1181A'
         placeholder="CVV"
        
        />
        </View>
        <TextInput
        style={{marginTop: 20, height: 40, width: 300,  color: '#B1181A'}}
        placeholderTextColor='#D8D8D8'
         underlineColorAndroid='#B1181A'
         placeholder="xxxx xxxx xxxx xxxx"
        
        />

        </View>

        </View>

        
        <View style={{ padding: 10, height: 60, backgroundColor: 'white', justifyContent: 'space-between', flexDirection: 'row' }}>
                  <TouchableOpacity  style={styles.button}>
                    <Text style={{  color: 'white'}}>Guardar <Icon name='shopping-cart' size={18} style={{color:'white'}}/></Text>
                </TouchableOpacity>
                <TouchableOpacity  onPress={() => this._toggleModal()}  style={{   borderColor: '#B1181A', borderWidth: 2, alignItems: 'center', justifyContent: 'center', borderRadius: 1, elevation: 3, height: 40, width: 100,  backgroundColor: 'white',}}>
                    <Text style={{   color: '#B1181A'}}>Cerrar</Text>
                </TouchableOpacity>
        </View>
      </Modal>
                 
     
     </View>

     

    );
  }
}



const styles = StyleSheet.create({
    wrapper: {
    },
    button: {
        borderColor: '#B1181A',
         borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 1,
         elevation: 3,
          height: 40,
           width: 150, 
            
            
            backgroundColor: '#B1181A',
            
      },
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#9DD6EB',
    },
    slide2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#97CAE5',
    },
    slide3: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#92BBD9',
    },
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold',
    }
  })
  