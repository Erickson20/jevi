import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView
  
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import ProductBox from '../product/ProductBox';
import {Actions} from 'react-native-router-flux'




export default class Seach extends Component {
    

  render() {
  
    
    return (
     <View style={{backgroundColor: 'white', flex: 1, flexDirection: 'column'}}>
   
     <View style={{ padding: 5, backgroundColor: '#B1181A', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', flex: 1}}>
     <TouchableOpacity transparent onPress={() => Actions.pop()} >
         <Icon name='arrow-left' size={20} style={{   color:'white'}}/>
     </TouchableOpacity>
     <TextInput
        style={{height: 38, width: 260, backgroundColor: 'white', color: 'black', borderColor: 'white', borderWidth: 2  }}
        placeholderTextColor='black'
        underlineColorAndroid='white'
        placeholder="Busca una bebida..." 
     >
     
     

     </TextInput>
     <Icon name='search' size={20} style={{   color:'white'}}/>
     </View>
     <View style={{marginTop: 10, backgroundColor: 'white', flexDirection: 'column', justifyContent: 'center', flex: 13}}>
     </View>

     

     
                 
     
     </View>
    );
  }
}



const styles = StyleSheet.create({
    wrapper: {
    },
    button: {
        borderColor: '#B1181A',
         borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 1,
         elevation: 3,
          height: 40,
           width: 150, 
            marginTop: 40,
            marginLeft: 5,
            backgroundColor: '#B1181A',
            
      },
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#9DD6EB',
    },
    slide2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#97CAE5',
    },
    slide3: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#92BBD9',
    },
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold',
    }
  })
  