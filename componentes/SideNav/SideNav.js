import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  ImageBackground
} from 'react-native';

import {Actions} from 'react-native-router-flux'
import Modal from "react-native-modal";
import Icon from 'react-native-vector-icons/FontAwesome';



export default class SideNav extends Component {

 
  render() {
    return (
        <View style={{flex: 1, backgroundColor: '#B1181A'}}>
           
           
           <View style={{flex: 2, backgroundColor: '#B1181A', flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
           
           <Image
           style={{ width: 100, height: 100,  borderRadius: 15}}
           source={{uri: 'https://profile.actionsprout.com/default.jpeg'}}
           />
           <Text style={{color: 'white', fontSize: 16}}>Erickson FLores</Text>
           <Text style={{ color: 'white' ,fontSize: 13}}>ericksonflores20@gmail.com</Text>
           </View>
           <View style={{flex: 3, backgroundColor: 'white', justifyContent: 'space-around', flexDirection: 'column', padding: 10}}>
           <Text style={{ fontSize: 18}}><Icon name='edit' size={20} style={{color:'#B1181A'}}/> Editar perfil</Text>
           <Text style={{ fontSize: 18}}><Icon name='history' size={20} style={{color:'#B1181A'}}/> Historial de compra</Text>
           <Text style={{ fontSize: 18}}><Icon name='book' size={20} style={{color:'#B1181A'}}/> Politicas de uso</Text>
           <Text style={{ fontSize: 18}}><Icon name='question' size={20} style={{color:'#B1181A'}}/> Ayuda</Text>
           <Text style={{ fontSize: 18}}><Icon name='phone' size={20} style={{color:'#B1181A'}}/> Contactanos</Text>
           <Text style={{ fontSize: 18}}><Icon name='sign-out' size={20} style={{color:'#B1181A'}}/> Cerrar sesion</Text>


           </View>
           <View style={{flex: 1, backgroundColor: 'white'}}>
           </View>
           
          
           
        </View>
        
    );
  }

  

 

}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
   
  },
  button: {
    borderColor: '#B1181A',
     borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 1,
     elevation: 3,
      height: 40,
       width: 150, 
        
        backgroundColor: '#B1181A',
        
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
