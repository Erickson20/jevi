import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView
  
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import ProductBox from '../product/ProductBox';
import {Actions} from 'react-native-router-flux'




export default class Detail extends Component {
    

  render() {
  
    
    return (
     <View style={{backgroundColor: 'white', flex: 1, flexDirection: 'column'}}>
         <View style={{ backgroundColor: '#D8D8D8', padding: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{fontSize: 15, color: '#B1181A'}}>Cliente:</Text>
            <Text style={{fontSize: 15}}>Erickson Flores</Text>
         </View>
         <View style={{ padding: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{fontSize: 15, color: '#B1181A'}}>Cedula:</Text>
            <Text style={{fontSize: 15}}>40200431348</Text>
         </View>
         <View style={{backgroundColor: '#D8D8D8', padding: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{fontSize: 15, color: '#B1181A'}}>ID Orden:</Text>
            <Text style={{fontSize: 15}}>#09678123</Text>
         </View>
         <View style={{ padding: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{fontSize: 15, color: '#B1181A'}}>Productos:</Text>
            <Text style={{fontSize: 15}}>barcelo, wisky 12 años</Text>
         </View>
         <View style={{backgroundColor: '#D8D8D8', padding: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{fontSize: 15, color: '#B1181A'}}>Fecha:</Text>
            <Text style={{fontSize: 15}}>12/12/12 8:00 PM</Text>
         </View>
         <View style={{ padding: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{fontSize: 15, color: '#B1181A'}}>Delivery:</Text>
            <Text style={{fontSize: 15}}>RD$ 100.00</Text>
         </View>
         <View style={{backgroundColor: '#D8D8D8', padding: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{fontSize: 15, color: '#B1181A'}}>Total:</Text>
            <Text style={{fontSize: 15}}>RD$ 5000.00</Text>
         </View>
         <View style={{ padding: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{fontSize: 15, color: '#B1181A'}}>Tiempo de llegada:</Text>
            <Text style={{fontSize: 15}}>5 minutos</Text>
         </View>
         <View style={{ backgroundColor: '#D8D8D8', padding: 10, flexDirection: 'column',}}>
            <Text style={{fontSize: 15, color: '#B1181A'}}>estas en:</Text>
            <Image
                style={{ borderRadius: 4, marginTop: 10, width: 340, height: 110}}
                source={{uri: 'https://firebasestorage.googleapis.com/v0/b/jevi-9acff.appspot.com/o/logo%2FCaptura.PNG?alt=media&token=36f1329b-2cb2-429b-8eb0-6cd01a9dc38d'}}
            />
         </View>

         <View style={{marginTop: 10, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'center', flex: 1}}>
        <TouchableOpacity onPress={() => Actions.Location()}  style={styles.button}>
            <Text style={{  color: 'white'}}>Confirmar compra</Text>
        </TouchableOpacity>
        </View>
         
         
     
     </View>
    );
  }
}



const styles = StyleSheet.create({
    wrapper: {
    },
    button: {
        borderColor: '#B1181A',
         borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 1,
         elevation: 3,
          height: 40,
           width: 150, 
           
            marginLeft: 5,
            backgroundColor: '#B1181A',
            
      },
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#9DD6EB',
    },
    slide2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#97CAE5',
    },
    slide3: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#92BBD9',
    },
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold',
    }
  })
  