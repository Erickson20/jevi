import React, { Component } from 'react';
import { 
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Right
 } from 'react-native';

 import { Tabs, Actions, Scene, Router, Modal, Drawer} from 'react-native-router-flux';
 import Icon from 'react-native-vector-icons/FontAwesome';

 import Login from './componentes/login/login';
 import Register from './componentes/register/Register';
 import Forget from './componentes/ForgetPass/forget';
 import Inicio from './componentes/Inicio/inicio';
 import Home from './componentes/Tabs/TabsHome/Home';
 import SideBar from './componentes/SideNav/SideNav';
 import Category from './componentes/categoria/Category';
 import NotificationList from './componentes/notification/NotificationList';
 import Cart from './componentes/Cart/Cart';
 import Seach from './componentes/seach/Seach';
 import Location from './componentes/location/Location';
 import Payment from './componentes/Pay/Payment';
 import Detail from './componentes/Detail/Detail';
 


 const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
const myIcon = (<Icon name="rocket" size={30} color="#900" />)

export default class App extends Component {
  render() {
    return (
    
       
      <Router >
      <Scene key="root" hideNavBar >
        <Scene key="auth">
        <Scene key="Login" component={Login} title="Login" hideNavBar />
        <Scene key="Register" component={Register} title="Registro" navigationBarStyle={{backgroundColor: '#B1181A'}} navBarButtonColor="white"  />
        <Scene key="Forget" component={Forget} title="Restauracion de credenciales" navigationBarStyle={{backgroundColor: '#B1181A'}} navBarButtonColor="white"  />
     
        </Scene>
      <Drawer hideNavBar key="drawerMenu" contentComponent={SideBar} drawerWidth={250} drawerPosition="left" drawerIcon ={() => <Icon name='ios-menu-outline' size={18} style={{color:'white',  marginRight: 20 }}/>} >
       <Scene key="Inicio"   title="Inicio" 
          

        renderRightButton = {() => 
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity transparent onPress={() => Actions.Notification()} >
            <Icon name='bell' size={18} style={{color:'white',  marginRight: 20 }}/>
          </TouchableOpacity> 
          <TouchableOpacity transparent onPress={() => Actions.Seach()} >
            <Icon name='search' size={18} style={{color:'white',  marginRight: 20 }}/>
          </TouchableOpacity> 
          <TouchableOpacity transparent  onPress={() => Actions.Cart()}>
            <Icon name='shopping-cart' size={18} style={{color:'white',  marginRight: 20 }}/>
          </TouchableOpacity>           
        </View>

      } 
          title="Jevi"
          titleStyle={{color: 'white', alignContent: 'center'}}
          navigationBarStyle={{backgroundColor: '#B1181A', elevation: 0, bottom: 0, paddingTop: 20}}   > 
          
            <Scene key="Home"  component={Inicio} />

            <Scene key="Category" 
              renderLeftButton = {() =>  
                <View>
                <TouchableOpacity transparent onPress={() => Actions.pop()} >
                  <Icon name='arrow-left' size={17} style={{color:'white', marginLeft: 20 }}/>
                </TouchableOpacity>
                </View>
              
              } 
          component={Category} />


          <Scene key="Notification" 
            renderLeftButton = {() =>  
              <View>
              <TouchableOpacity transparent onPress={() => Actions.pop()} >
                <Icon name='arrow-left' size={17} style={{color:'white', marginLeft: 20 }}/>
              </TouchableOpacity>
              </View>
            
            } 
          component={NotificationList} />
          <Scene key="Cart" 
          renderLeftButton = {() =>  
            <View>
            <TouchableOpacity transparent onPress={() => Actions.pop()} >
              <Icon name='arrow-left' size={17} style={{color:'white', marginLeft: 20 }}/>
            </TouchableOpacity>
            </View>
          
          } 
           component={Cart} title="Cart"  />

          
      </Scene>
      </Drawer>
      <Scene key="Seach" component={Seach} title="Buscar"  hideNavBar />

      
      
      <Scene key="Buy" titleStyle={{color: 'white'}} navBarButtonColor="white" navigationBarStyle={{backgroundColor: '#B1181A', elevation: 0, bottom: 0}}>
      
      <Scene key="Location" component={Location} title="A donde va la order?" />
      <Scene key="Payment" component={Payment} title="Cual es la forma de pago?" />
      <Scene key="Detail" component={Detail} title="Detalle de compra" />


      </Scene>

      
      

      
      
      </Scene>
      
    </Router>
    
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
